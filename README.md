## Uzil.InputPipe

節錄並展示部分工具集功能

### 原因
因之前專案中有特殊需求，需要更程序化/簡化/可攜的使用...等，故不採用InputSystem而是另外撰寫輸入管理系統來使用。

### 功能
* 提供 Layer分層，功能類似ActionMap。
* 註冊 處理器Handler，把 實際的輸入 轉換為 虛擬輸入。(就像keycode -> action)
* 註冊 偵聽者OnInputListener 提供被動事件偵聽。<br>可排序優先度，且若有需要可截斷/取消使訊號不繼續傳遞給下個Layer/Listener。
* 與ToLua共同使用，從Lua層對操作輸入進行使用。

### 說明
1. 建立 Layer 與 Handler，對實際Input進行監聽。
2. 註冊 OnInputListener 被動偵聽。
3. 實際Input 發生後，經由Handler轉換成欲使用的 虛擬Input。
4. 虛擬Input 會依優先度發送給該Layer的各個Listener，中途可截斷/取消事件傳遞給下一個Layer/Listener。
