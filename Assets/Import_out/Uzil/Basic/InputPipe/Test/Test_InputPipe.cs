using System.Collections.Generic;
using UnityEngine;

using Uzil.Util;
using Uzil.Mod;
using Uzil.Lua;


namespace Uzil.InputPipe {
public class Test_InputPipe : MonoBehaviour {

	
	/*======================================Constructor==========================================*/

	/*=====================================Static Members========================================*/

	/*=====================================Static Funciton=======================================*/

	/*=========================================Members===========================================*/

	InputMgr inputMgr;

	public TextAsset textAsset;

	public bool isStopSignal_87SrcKey = false;

	/*========================================Components=========================================*/

	/*==========================================Event============================================*/

	/*========================================Interface==========================================*/

	/*======================================Unity Function=======================================*/

	void Awake () {

		InputUtil.LogAllKeyCode();

		// 取得 實例
		this.inputMgr = InputMgr.Inst("_test");

		InputLayer layer = new InputLayer("_testLayer", 1);
		
		// 新增 轉換器

		// Q W 同時按 得到的 87 應該以 來源W 為 優先
		layer.AddHandler(new InputHandler_KeyConvert(87, new List<int>{
			(int) KeyCode.Q
		}).Priority(2));
		layer.AddHandler(new InputHandler_KeyConvert(87, new List<int>{
			(int) KeyCode.W
		}).Priority(3));


		// 來源A 可同時被視為 87 與 88 
		layer.AddHandler(new InputHandler_KeyConvert(87, new List<int>{
			(int) KeyCode.A
		}));
		layer.AddHandler(new InputHandler_KeyConvert(88, new List<int>{
			(int) KeyCode.A, (int) KeyCode.S
		}));
	
		// 新增 層級
		this.inputMgr.AddLayer(layer);	

		// Lua
		ModMgr modMgr = ModMgr.Inst();
		modMgr.AddBaseLoader();
		modMgr.ReloadAll();

		LuaUtil.DoString(this.textAsset.text);


		
		// 被動註冊事件 ========

		// 當 87 Vkey 有 訊號
		layer.AddOnInput(87, new EventListener((data)=>{
			InputSignal signal = (InputSignal) data.Get("signal");
			Debug.Log("OnInput[87]: "+((InputKeyState)signal.GetValueNum()).ToString("F")+" / "+((KeyCode)signal.realKeyCode).ToString("F"));

			// 可選 是否截斷訊號 不讓 88 拿到
			signal.Stop(this.isStopSignal_87SrcKey);
		}));

		// 當 88 Vkey 有 訊號
		layer.AddOnInput(88, new EventListener((data)=>{
			InputSignal signal = (InputSignal) data.Get("signal");
			Debug.Log("OnInput[88]: "+((InputKeyState)signal.GetValueNum()).ToString("F")+" / "+((KeyCode)signal.realKeyCode).ToString("F"));
		}));
	}

	void Update () {

		// 主動取得 操作輸入
		InputSignal signal = this.inputMgr.GetLayer("_testLayer").GetInput(87);
		if (signal == null) return;
		Debug.Log("Update:"+((InputKeyState)signal.GetValueNum()).ToString("F"));
		
	}

	/*=====================================Public Function=======================================*/

	/*===================================Protected Function======================================*/
	
	/*====================================Private Function=======================================*/
}


}
