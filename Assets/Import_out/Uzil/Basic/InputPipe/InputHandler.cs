using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 輸入Input處理
 * 接收 實際Input 並轉換為 虛擬Input
 */

namespace Uzil.InputPipe {
public class InputHandler {

	
	/*======================================Constructor==========================================*/

	public InputHandler () {}

	public InputHandler (string id) {
		this.id = id;
	}

	/*=====================================Static Members========================================*/

	/*=====================================Static Funciton=======================================*/

	/*=========================================Members===========================================*/

	/** 識別 */
	public string id {get; protected set;} = null;

	/** 優先度 */
	public float priority = 0f;

	/** 來源 KeyCode */
	public List<int> srcKeyCodes = new List<int>();

	/*========================================Components=========================================*/

	/*==========================================Event============================================*/

	/*========================================Interface==========================================*/

	/*=====================================Public Function=======================================*/

	/** 處理信號 */
	public virtual void HandleSignal (InputSignal signal) {

	}

	/** 設置 資料 */
	public virtual void SetData (DictSO data) {
		if (data == null) return;

		data.TryGetList<int>("src", (res)=>{
			if (res != null) this.srcKeyCodes = res;
		});

		data.TryGetFloat("priority", (res)=>{
			this.priority = res;
		});
	}

	public InputHandler ID (string id) {
		if (this.id != null) {
			Debug.LogError("[InputHandler] id exist, can't be change.");
			return this;
		}
		this.id = id;
		return this;
	}

	/** 設置 優先度 */
	public InputHandler Priority (float priority) {
		this.priority = priority;
		return this;
	}

	/*===================================Protected Function======================================*/
	
	/*====================================Private Function=======================================*/
}


}
