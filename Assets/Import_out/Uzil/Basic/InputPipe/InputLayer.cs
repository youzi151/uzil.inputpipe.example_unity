using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UzEvent = Uzil.Event;

/**
 * 
 */

namespace Uzil.InputPipe {

public class InputLayer {

	
	/*======================================Constructor==========================================*/

	public InputLayer (string id, float priority) {
		this.id = id;
		this.priority = priority;
	}

	/*=====================================Static Members========================================*/

	/*=====================================Static Funciton=======================================*/

	/*=========================================Members===========================================*/

	/** 識別 */
	public string id {get; protected set;}

	/** 優先度 */
	public float priority = 0f;

	/** 是否啟用 */
	public bool isActive { get; private set; } = true;

	/** 處理器 */
	protected List<InputHandler> handlers = new List<InputHandler>();

	/*========================================Components=========================================*/

	/*==========================================Event============================================*/

	/** 每個用到的 虛擬輸入 */
	protected List<int> vkeys = new List<int>();

	/** 虛擬輸入 對應 事件 */
	protected Dictionary<int, UzEvent> vkey2Event = new Dictionary<int, UzEvent>();

	/** 虛擬輸入 對應 持有/當前的訊號 */
	protected List<InputSignal> signals = new List<InputSignal>();
	protected Dictionary<int, InputSignal> vkey2Signal = new Dictionary<int, InputSignal>();
	
	/*========================================Interface==========================================*/

	/*=====================================Public Function=======================================*/	

	/** 啟用 */
	public void Active () {
		this.isActive = true;
	}

	/** 關閉 */
	public void Deactive () {
		this.isActive = false;
	}

	//  ######  ####  ######   ##    ##    ###    ##       
	// ##    ##  ##  ##    ##  ###   ##   ## ##   ##       
	// ##        ##  ##        ####  ##  ##   ##  ##       
	//  ######   ##  ##   #### ## ## ## ##     ## ##       
	//       ##  ##  ##    ##  ##  #### ######### ##       
	// ##    ##  ##  ##    ##  ##   ### ##     ## ##       
	//  ######  ####  ######   ##    ## ##     ## ######## 

	/** 處理信號 */
	public void HandleSignal (InputSignal signal) {
		// 每個 Handler
		foreach (InputHandler each in this.handlers) {
			InputHandler handler = each;
			InputSignal copySignal = signal.GetCopy();

			// 把 信號 的 來源key 處理為 虛擬key
			handler.HandleSignal(copySignal);

			// 轉紀錄 虛擬key

			// 若存在 則 移除已存在的信號 並 取代
			if (this.vkey2Signal.ContainsKey(copySignal.virtualKeyCode)) {
				
				InputSignal last = this.vkey2Signal[copySignal.virtualKeyCode];
				this.signals.Remove(last);

				this.vkey2Signal[copySignal.virtualKeyCode] = copySignal;

			} else {
				this.vkey2Signal.Add(copySignal.virtualKeyCode, copySignal);
			}

			this.signals.Add(copySignal);
		}
	}

	/** 取得信號 */
	public InputSignal GetSignal (int virtualKeyCode) {
		if (this.vkey2Signal.ContainsKey(virtualKeyCode) == false) return null;
		InputSignal signal = this.vkey2Signal[virtualKeyCode];
		return signal;
	}

	/** 清空信號 */
	public void ClearSignals () {
		this.vkey2Signal.Clear();
		this.signals.Clear();
	}

	// ##     ##    ###    ##    ## ########  ##       ######## ########  
	// ##     ##   ## ##   ###   ## ##     ## ##       ##       ##     ## 
	// ##     ##  ##   ##  ####  ## ##     ## ##       ##       ##     ## 
	// ######### ##     ## ## ## ## ##     ## ##       ######   ########  
	// ##     ## ######### ##  #### ##     ## ##       ##       ##   ##   
	// ##     ## ##     ## ##   ### ##     ## ##       ##       ##    ##  
	// ##     ## ##     ## ##    ## ########  ######## ######## ##     ## 

	/** 取得 處理器 */
	public List<InputHandler> GetHandlers () {
		return this.handlers;
	}

	/** 取得 處理器 */
	public InputHandler GetHandler (string handlerID) {
		foreach (InputHandler each in this.handlers) {
			if (each.id == handlerID) return each;
		}
		return null;
	}

	/** 新增 處理器 */
	public void AddHandler (InputHandler handler, bool isSort = true) {
		if (handler.id != null) {
			InputHandler exist = this.GetHandler(handler.id);
			if (exist != null) {
				this.RemoveHandler(handler);
			}
		}
		this.handlers.Add(handler);
		if (isSort) this.SortHandler();
	}

	/** 新增 處理器 */
	public void AddHandler (List<InputHandler> toAddHandlers) {
		for (int idx = 0; idx < toAddHandlers.Count; idx++) {
			this.AddHandler(toAddHandlers[idx], false);
		}
		this.SortHandler();
	}

	/** 排序 處理器 */
	public void SortHandler () {
		this.handlers.Sort((a, b)=>{
			return a.priority.CompareTo(b.priority);
		});
	}

	/** 移除 處理器 */
	public void RemoveHandler (InputHandler handler) {
		if (this.handlers.Contains(handler) == false) return;
		this.handlers.Remove(handler);
	}

	/** 移除 處理器 */
	public void RemoveHandler (string handlerID) {
		InputHandler exist = this.GetHandler(handlerID);
		if (exist == null) return;
		this.handlers.Remove(exist);
	}

	// #### ##    ## ########  ##     ## ######## 
	//  ##  ###   ## ##     ## ##     ##    ##    
	//  ##  ####  ## ##     ## ##     ##    ##    
	//  ##  ## ## ## ########  ##     ##    ##    
	//  ##  ##  #### ##        ##     ##    ##    
	//  ##  ##   ### ##        ##     ##    ##    
	// #### ##    ## ##         #######     ##    

	/** 查詢 */
	public InputSignal GetInput (int virtualKeyCode) {
		if (this.vkey2Signal.ContainsKey(virtualKeyCode) == false) return null;
		return this.vkey2Signal[virtualKeyCode];
	}

	/** 註冊 當 輸入 */
	public void AddOnInput (int virtualKeyCode, EventListener eventListener) {
		UzEvent onInput;
		if (this.vkey2Event.ContainsKey(virtualKeyCode) == false) {
			onInput = new UzEvent();
			this.vkey2Event.Add(virtualKeyCode, onInput);
			this.vkeys.Add(virtualKeyCode);
			this.vkeys.Sort();
		} else {
			onInput = this.vkey2Event[virtualKeyCode];
		}
		onInput.AddListener(eventListener);
	}

	/** 註銷 當 輸入 */
	public void RemoveOnInput (int virtualKeyCode, EventListener eventListener) {
		if (this.vkey2Event.ContainsKey(virtualKeyCode) == false) return;
		UzEvent onInput = this.vkey2Event[virtualKeyCode];
	}
	/** 註銷 當 輸入 */
	public void RemoveOnInput (int virtualKeyCode, string eventListenerID) {
		if (this.vkey2Event.ContainsKey(virtualKeyCode) == false) return;
		UzEvent onInput = this.vkey2Event[virtualKeyCode];
		onInput.Remove(eventListenerID);
	}

	/** 呼叫 信號 */
	public void CallInput (InputSignal signal) {
		// 若已經被中止 則 返回
		if (signal.IsAlive() == false) return;
		// 防呆
		if (this.vkey2Event.ContainsKey(signal.virtualKeyCode) == false) return;
		// 呼叫
		this.vkey2Event[signal.virtualKeyCode].Call(new DictSO().Ad("signal", signal));
	}

	public void CallAllInput () {
		foreach (int vkey in this.vkeys) {
			
			if (this.vkey2Signal.ContainsKey(vkey) == false) continue;

			InputSignal signal = this.vkey2Signal[vkey];
			this.CallInput(signal);
		}
	}

	
	/** 
	 * 停止輸入 
	 * 以最終的虛擬KeyCode，向該層級取得訊號後執行停止，追溯到源訊號將實際KeyCode關閉
	 * @param isStream 是否
	 */
	public void StopInput (int virtualKeyCode, bool isStream) {
		InputSignal signal = this.GetSignal(virtualKeyCode);
		if (signal == null) return;
		signal.Stop(isStream);
	}


	/*===================================Protected Function======================================*/
	
	/*====================================Private Function=======================================*/
}


}
